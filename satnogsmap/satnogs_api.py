import requests
from datetime import datetime
from skyfield.api import utc
from satnogs_api_client.satnogs_api_client import get_paginated_endpoint

def getSatellites():
    transmitters = requests.get("https://db.satnogs.org/api/transmitters?format=json").json()
    satellites = requests.get("https://db.satnogs.org/api/satellites?format=json").json()
    sat = {}
    for x in satellites:
        sat[x["norad_cat_id"]] = x["name"]  #Get satellite name from DB
    Satellites = {}
    for x in transmitters:  #Use Transmitter uuid to make looking up satellite from job easier
        if x["alive"]:
            temp = {}
            temp["name"] = sat[x["norad_cat_id"]]
            temp["description"] = x["description"]
            temp["norad_cat_id"] = x["norad_cat_id"]
            temp["service"] = x["service"]
            temp["mode"] = x["mode"]
            Satellites[x["uuid"]] = temp
    return Satellites

def getGroundStations():
    stations = get_paginated_endpoint("https://network.satnogs.org/api/stations/?format=json")
    Stations = {}
    for x in stations:
        temp = {}
        temp["id"] = x["id"]
        temp["location"] = [x["lng"],x["lat"],x["altitude"]]
        temp["status"] = x["status"]
        temp["name"] = x["name"]
        Stations[x["id"]] = temp
    return Stations

def getJobs(satellites):
    jobs = requests.get("https://network.satnogs.org/api/jobs").json()
    jobs.reverse()
    passes = {}
    for x in jobs:
        #print(type(x),x)
        if not x["transmitter"] in satellites.keys():
            continue
        norad = satellites[x["transmitter"]]["norad_cat_id"]
        if not norad in passes.keys():
            passes[norad] = [{"tle0":x["tle0"],"tle1":x["tle1"],"tle2":x["tle2"],"start":convertTime(x["start"]),"end":convertTime(x["end"])}]
            passes[norad][-1]["stations"] = [{"ground_station":x["ground_station"],"transmitter":x["transmitter"],"start":convertTime(x["start"]),"end":convertTime(x["end"])}]
        else:
            if (passes[norad][-1]["end"] - convertTime(x["start"])).total_seconds() > -900:
                passes[norad][-1]["end"] = convertTime(x["end"])
            else:
                passes[norad].append({"tle0":x["tle0"],"tle1":x["tle1"],"tle2":x["tle2"],"start":convertTime(x["start"]),"end":convertTime(x["end"]),"stations":[]})
            passes[norad][-1]["stations"].append({"ground_station":x["ground_station"],"transmitter":x["transmitter"],"start":convertTime(x["start"]),"end":convertTime(x["end"])})

    return passes

def convertTime(time):
    return datetime.strptime(time,"%Y-%m-%dT%H:%M:%SZ").astimezone(utc)
