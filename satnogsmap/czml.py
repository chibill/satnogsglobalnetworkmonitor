import json
from skyfield.api import EarthSatellite, utc, load
from numpy import isnan
from datetime import datetime, timedelta

ts = load.timescale()

def makeStation(id,station):
    Station = {}
    Station["id"] = id
    Station["name"] = station["name"]
    Station["show"] = True
    Station["point"] = {"color":{"rgba":[0,230,64,255]},"outlineColor":{"rgba":[255,255,255,128]},"outlineWidth":2.0}
    Station["position"] = {"cartographicDegress":station["location"]}
    if station["status"] == "Testing":
        Station["point"]["color"]["rgba"] = [248,148,6,220]
    if station["status"] == "Offline":
       Station["point"]["color"]["rgba"] = [255,0,0,15]
    return Station

def makeStations(stations):
    Stations = []
    for x in stations.keys():
        station = makeStation(x,stations[x])
        Stations.append(station)

    return Stations

def makePass(satPass,updateInterval = 60):
    sat = EarthSatellite(satPass["tle1"],satPass["tle2"],satPass["tle0"])
    pointTime = satPass["start"]
    czmlPointTime = 0
    updateIntervalDelta = timedelta(seconds=updateInterval)
    path = []
    while pointTime <= satPass["end"]:
        subpoint = sat.at(ts.utc(pointTime)).subpoint()
        lat = subpoint.latitude.degrees
        lng = subpoint.longitude.degrees
        if isnan(lat):
            return None
        elevation = subpoint.elevation.m
        path.extend([czmlPointTime,lng,lat,elevation])
        czmlPointTime += updateInterval
        pointTime = pointTime + updateIntervalDelta
    Pass = {}
    Pass["id"] = satPass["tle0"] + satPass["start"].isoformat()
    Pass["name"] = satPass["tle0"]
    Pass["show"] = True
    Pass["billboard"] = {"image":"static/sat.png","scale":0.5}
    Pass["position"] = {"cartographicDegrees":path, "interpolationAlgorithm": "LAGRANGE","interpolationDegree":5,"eproch":(satPass["start"].isoformat()+"Z").replace("+00:00","")}
    interval = (satPass["start"].isoformat()+"Z").replace("+00:00","") + "/" +(satPass["end"].isoformat()+"Z").replace("+00:00","")
    material = {"solidColor":{"color":{"rgba":[0,255,0,255]}}}
    Pass["path"] = {"show":{"interval":interval,"boolean":True},"width":2,"material":material,"leadTime":100000,"trailTime":10000}
    return Pass

def makeLinks(satPass):
    Links = []
    for x in satPass["stations"]:
       link = {}
       link["id"] = x["name"]+satPass["tle0"]+satPass["start"].isoformat()
       link["polyline"] = {}
       link["ployline"]["show"] = {"interval":(x["start"].isoformat()+"Z").replace("+00:00","") + "/" + (x["end"].isoformat()+"Z").replace("+00:00","") , "boolean": True}
       link["polyline"]["width"] = 2
       link["polyline"]["material"] = {"soildColor":{"color":{"rgba":[255,0,0,255]}}}
       link["polyline"]["followSurface"] = False
       link["ployline"]["positions"] = {"references":[satPass["tle0"]+satPass["start"].isoformat()+"#position",str(x["id"])+"#position"]}
       Links.append(link)
    return Links
    
def makeDocument(gs,jobs):
    Document = []
    header = {"id":"document","name":"display","version":"1.0","clock":{"interval":"0000-00-00T00:00:00Z/9999-12-31T24:00:00Z","step":"SYSTEM_CLOCK"}
    Document.append(header)
    Document.extend(makeStations(gs))
    for satellite in jobs.keys():
        for satPass in jobs[satellite]:
            Document.append(makePass(satPass))
            Document.append(makeLinks(satPass))
    return Document
            
    
