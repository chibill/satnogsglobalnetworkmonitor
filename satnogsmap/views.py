from django.http import HttpResponse
from satnogs_api_client import fetch_satellites
from satnogs_api_client.satnogs_api_client import DB_BASE_URL, get_paginated_endpoint

import requests
import random
from skyfield.api import EarthSatellite, utc, load
from numpy import isnan

def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")
